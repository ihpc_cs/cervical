import os
from subprocess import call

from utility import make_square_2, gen_test_lists, decode_results_n, classify_and_submit, resize_input_imgs, train_model_aug

roi_dir = os.path.join(os.getcwd(), 'rois')
resnet_dir = os.path.join(os.getcwd(), 'resnet')
input_dir = os.path.join(os.getcwd(), 'input')

#prepare testing path
test_path = './tst_stg2'
dir_org = test_path
dir_dst = os.path.join(os.getcwd(), 'tst_stg2_resized')



def run_main_train():
	
	#step-1: prepare lists for yolo training
	#resize input images to square size and copy to ./yolo/cervical/images for training yolo models
	resize_input_imgs(input_dir)

	for kk in range(1, 4, 1):
		os.system('cp ./input/train_resized/Type_{:s}/*.jpg ./yolo/cervical/images/train'.format(str(kk)))            
		os.system('cp ./input/test_resized/Type_{:s}/*.jpg ./yolo/cervical/images/test'.format(str(kk)))  
		os.system('find `pwd`/input/train_resized/Type_{:s} -name \*.jpg > `pwd`/yolo/cervical/trn{:s}_txt.txt'.format(str(kk),str(kk)))
		os.system('find `pwd`/input/test_resized/Type_{:s} -name \*.jpg > `pwd`/yolo/cervical/tst{:s}_txt.txt'.format(str(kk),str(kk)))
		os.system('find `pwd`/input/addition_resized/Type_{:s} -name \*.jpg > `pwd`/yolo/cervical/add{:s}_txt.txt'.format(str(kk),str(kk)))
	os.chdir('yolo/cervical/')
	os.system('find `pwd`/images/train -name \*.jpg > train_org_txt.txt')
	os.system('find `pwd`/images/test -name \*.jpg > test_txt.txt')
	os.system('cat  train_org_txt.txt test_txt.txt  >train_txt.txt')
	os.chdir('../..')
    

	#step-2: train yolo models 
	os.chdir('yolo')
	print('Start yolo training............')
	for kk in range(1, 4, 1):
		os.system('./darknet detector train cfg/cervical.data cfg/cervical_final{:s}.cfg darknet19_448.conv.23 -gpus 0'.format(str(kk)))
		os.system('mv ./backup/cervical_final{:s}_final.weights ./model/cervical_f{:s}.weights'.format(str(kk), str(kk)))


	#step-3: detect cervicals by yolo; 
	print('Start rois detection............')

	#public test type1 to type3
	for rr in range(1,4,1):
		for kk in range(1, 4, 1):
			os.system('./darknet detector predict cfg/cervical_tst{:s}.data cfg/cervical_final{:s}.cfg model/cervical_f{:s}.weights -gpus 2,3'.format(str(rr),str(kk),str(kk)))    
			os.system('mv results.txt results_tst{:s}_{:s}.txt'.format(str(rr),str(kk)))

	#train type1 to type3
	for rr in range(1,4,1):
		for kk in range(1, 4, 1):
			os.system('./darknet detector predict cfg/cervical_trn{:s}.data cfg/cervical_final{:s}.cfg model/cervical_f{:s}.weights -gpus 2,3'.format(str(rr),str(kk),str(kk)))    
			os.system('mv results.txt results_trn{:s}_{:s}.txt'.format(str(rr),str(kk)))

	#addition type1 to type3
	for rr in range(1,4,1):
		for kk in range(1, 4, 1):
			os.system('./darknet detector predict cfg/cervical_add{:s}.data cfg/cervical_final{:s}.cfg model/cervical_f{:s}.weights -gpus 2,3'.format(str(rr),str(kk),str(kk)))    
			os.system('mv results.txt results_add{:s}_{:s}.txt'.format(str(rr),str(kk)))

	#step-4: extract rois
	#public test type1 to type3
	print('Start rois extraction............')
	for kk in range(1, 4, 1):
		strN = 'tst'+str(kk)+'_'
		decode_results_n(strN,1, 4, 1, 1.0, '../input/test_roi/Type_{:s}'.format(str(kk)))

        #train type1 to type3
	for kk in range(1, 4, 1):
		strN = 'trn'+str(kk)+'_'
		decode_results_n(strN,1, 4, 1, 1.0, '../input/train_roi/Type_{:s}'.format(str(kk)))

       #addition type1 to type3
	for kk in range(1, 4, 1):
		#os.system('rm ./roi -rf')
		strN = 'add'+str(kk)+'_'
		decode_results_n(strN,1, 4, 1, 1.0, '../input/addition_roi/Type_{:s}'.format(str(kk)))
		#os.system('cp ./roi ../input/addition_roi/Type_{:s} -rf'.format(str(kk)))
	os.chdir('..')

	#step-5: train resnet models
	#first set on trn+tst
	train_model_aug(30, 30, False)
	#second set on add+trn+tst
	train_model_aug(30, 30, True)

	os.system('mv *.h5 ./resnet')

    
    
	#post-process: trained yolo models and resnet models are used for run_main_test() on tst_stg2 image set. 





def run_main_test():
	#import pdb; pdb.set_trace()
	#step-1: resize images and record images list
	make_square_2(dir_org,dir_dst)
	gen_test_lists(dir_dst)


	#step-2: detect cervicals by yolo; 
	###assume the yolo package './yolo' located in current working path, and the trained yolo models are stored in './yolo/model'
	
	os.chdir('yolo')
	print('Start rois detection............')
	for kk in range(1, 4, 1):
		os.system('./darknet detector predict cfg/cervical.data cfg/cervical_final{:s}.cfg model/cervical_f{:s}.weights -gpus 3 -thresh 0.01'.format(str(kk),str(kk)))
		os.system('mv results.txt ../results_tst_{:s}.txt'.format(str(kk)))

	os.chdir('..')
	
	#step-3: extract rois
	decode_results_n('tst_',1, 4, 1, 1.0, roi_dir)



	#step-4: classify rois
	###assume the trained resnet50 models are stored in './resnet'
	classify_and_submit(1, 3, resnet_dir, roi_dir)

	#post-process: check and ensemble submission with the team

#train yolo and resnet models 
#run_main_train()
#test private image sets
run_main_test()
